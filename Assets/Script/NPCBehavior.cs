﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCBehavior : MonoBehaviour
{
    public NavMeshAgent _agenteNavigator;
    [SerializeField]
    private List<Transform> waypointListStored = new List<Transform>();
    [SerializeField]
    private List<Transform> waypointList= new List<Transform>();
    public Transform waypointAglomeracao;

    private int _indexWaypoint = 0;
    [SerializeField]
    private List<Transform> homepointList = new List<Transform>();
    private int _indexHomePoint = 0;

    private float _speed = 2;
    private bool _walking = true;

    private Animator npcAnimationController;
    public GameObject[] npcModels = null;
    public GameObject[] npcOldModels = null;

    public bool npcCanMove, canLookAt, isMasked;
    private int npcRandomIndex;
    private bool qteEvento = false;
    public bool _aglomerar;
    // Start is called before the first frame update
    void Start()
    {
        isMasked = false;

        //Logica de spawnar npcs velhos ou normais
        int npcModelRandom = Random.Range(1, 3);
        Debug.LogWarning(npcModelRandom);
        this.transform.position = waypointListStored[Random.Range(0, waypointListStored.Count)].position;
        //rodando logica de coleta de waypoints em um pool
        for (int i = 0; i < 4; i++)
        {
            waypointList.Add(waypointListStored[Random.Range(0, waypointListStored.Count)]);

        }

        if(npcModelRandom < 2)
        {
            Debug.LogWarning("Npc = 1");
            //Npcs normais
            int npcRandomIndex = Random.Range(0, npcModels.Length);
            npcModels[npcRandomIndex].SetActive(true);
            npcAnimationController = npcModels[npcRandomIndex].GetComponent<Animator>();
        }
        if(npcModelRandom >= 2)
        {
            Debug.LogWarning("Npc = 2");
            //Npcs velhos
            int npcOldRandomIndex = Random.Range(0, npcOldModels.Length);
            npcOldModels[npcOldRandomIndex].SetActive(true);
            npcAnimationController = npcOldModels[npcOldRandomIndex].GetComponent<Animator>();
        }

        this._indexHomePoint = Random.Range(0, homepointList.Count - 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMasked)
        {
            GoToHome();
        }
        else 
        {
            MoveToWaypoints();


        }
        //checar walking
        if (_agenteNavigator.isActiveAndEnabled && _agenteNavigator.isStopped && Input.GetKeyDown(KeyCode.F))
        {
            PutMask();
        }


        if (qteEvento) 
            if(Input.GetKeyDown(KeyCode.G))
            {
                StatsPlayer itemManagerCharacter = GameObject.FindGameObjectWithTag("ItemManager").GetComponent<StatsPlayer>();

                if (itemManagerCharacter.GetMaskStored() > 0)
                {

                    UIManager ui = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
                   
                    ui.Qte();
                    ui.HideButtonPress();
                    itemManagerCharacter.DecresceAlcool();
                    Time.timeScale = 1;
                    qteEvento = false;

                }

            }



    }

    private void GoToHome()
    {

        this.transform.LookAt(_agenteNavigator.nextPosition);
        _agenteNavigator.SetDestination(homepointList[_indexHomePoint].position);
        //this.transform.position = Vector3.MoveTowards(transform.position, homepointList[_indexHomePoint].position, _speed * Time.deltaTime);
        npcAnimationController.SetBool("Walking", true);
        if (Vector3.Distance(this.transform.position, homepointList[_indexHomePoint].position)<1)
        {
            Destroy(this.gameObject);
        }

    }

    public void MoveToWaypoints()
    {   //walking

        if (this._aglomerar)
        {
            if (Vector3.Distance(this.transform.position, waypointAglomeracao.position) > 4)
            {
                _agenteNavigator.SetDestination(waypointAglomeracao.position);
                npcAnimationController.SetBool("Walking", true);

            }
            else
            {
                npcAnimationController.SetBool("Idle", true);

                npcAnimationController.SetBool("Walking", false) ;
                _agenteNavigator.Stop();
                _aglomerar = false;
            }
               

        }
        else
        if (!_agenteNavigator.isStopped)
        {

            //this.transform.LookAt(waypointList[_indexWaypoint].position);
            this.transform.LookAt(_agenteNavigator.nextPosition);
            _agenteNavigator.SetDestination(waypointList[_indexWaypoint].position);
            //this.transform.position = Vector3.MoveTowards(transform.position, waypointList[_indexWaypoint].position, _speed * Time.deltaTime);
            npcAnimationController.SetBool("Walking", true);
            
            //if (this.transform.position == waypointList[_indexWaypoint].position)
            if(Vector3.Distance(this.transform.position,waypointList[_indexWaypoint].position)<1)
                _indexWaypoint++;
            if (_indexWaypoint >= waypointList.Count) _indexWaypoint = 0;

        }
        //npcAnimationController.SetBool("Walking", true);


    }
    private void OnTriggerEnter(Collider other)
    {

        if (!isMasked)
        {
            _walking = false;
            
            npcAnimationController.SetBool("Walking", false);
            
            _agenteNavigator.Stop();
            
            this.transform.LookAt(other.transform.position);
            if (Random.Range(0, 100) < 10)
            {

                Debug.Log("entrou na chance de 10%");
                qteEvento = true;
                DisplayMessengerButtonPress();
                Time.timeScale = 0.1f;

                StartCoroutine(Wait());
            }
        }


       

    }
    IEnumerator Wait()
    {

        yield return new WaitForSeconds(0.3f);
        if (qteEvento)
        {
            StatusEffectPlayer sep = GameObject.FindGameObjectWithTag("Player").GetComponent<StatusEffectPlayer>();
            sep.PermantDmgMultiply();
        }
        Time.timeScale = 1;

    }
    private void OnTriggerExit(Collider other)
    {
        _walking = true;
        _agenteNavigator.Resume();
    }



    private void PutMask()
    {
        if (!isMasked&&!_walking)
        {
            
            StatsPlayer itemManagerCharacter = GameObject.FindGameObjectWithTag("ItemManager").GetComponent<StatsPlayer>();
            NPCMask npcMaskRef = GetComponentInChildren<NPCMask>();


            if (itemManagerCharacter.GetMaskStored() > 0)
            {

                itemManagerCharacter.AddPeopleSaved();
                itemManagerCharacter.DecresceMask();
                npcMaskRef.ActiveMask();
                isMasked = true;
                _walking = true;
                npcAnimationController.SetBool("Walking", true);
                npcAnimationController.SetBool("Idle", false);
                _agenteNavigator.Resume();
                DisplayMessengerOnScreen();
            }
            

        }

    }
    private void DisplayMessengerButtonPress()
    {
        UIManager ui = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        ui.ShowButtonPress();
    }
    private void DisplayMessengerOnScreen()
    {
        UIManager ui = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        ui.ShowMessage();
    }
}
