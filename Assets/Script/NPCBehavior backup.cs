﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class NPCBehavior : MonoBehaviour
//{

//    [SerializeField]
//    private List<Transform> waypointList = new List<Transform>();
//    private int _indexWaypoint = 0;
//    [SerializeField]
//    private List<Transform> homepointList = new List<Transform>();
//    private int _indexHomePoint = 0;

//    private float _speed = 2;
//    private bool _walking = true;

//    private Animator npcAnimationController;
//    public GameObject[] npcModels = null;
//    public GameObject[] npcOldModels = null;

//    public bool npcCanMove, canLookAt, isMasked;
//    private int npcRandomIndex;
//    private bool qteEvento = false;


//    // Start is called before the first frame update
//    void Start()
//    {
//        isMasked = false;
        
//        //Logica de spawnar npcs velhos ou normais
//        int npcModelRandom = Random.Range(1, 3);
//        Debug.LogWarning(npcModelRandom);

//        if(npcModelRandom < 2)
//        {
//            Debug.LogWarning("Npc = 1");
//            //Npcs normais
//            int npcRandomIndex = Random.Range(0, npcModels.Length);
//            npcModels[npcRandomIndex].SetActive(true);
//            npcAnimationController = npcModels[npcRandomIndex].GetComponent<Animator>();
//        }
//        if(npcModelRandom >= 2)
//        {
//            Debug.LogWarning("Npc = 2");
//            //Npcs velhos
//            int npcOldRandomIndex = Random.Range(0, npcOldModels.Length);
//            npcOldModels[npcOldRandomIndex].SetActive(true);
//            npcAnimationController = npcOldModels[npcOldRandomIndex].GetComponent<Animator>();
//        }

//        this._indexHomePoint = Random.Range(0, homepointList.Count - 1);
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (isMasked)
//        {
//            GoToHome();
//        }
//        else 
//        {
//            MoveToWaypoints();


//        }

//        if (!_walking && Input.GetKeyDown(KeyCode.F))
//        {
//            PutMask();
//        }


//        if (qteEvento) 
//            if(Input.GetKeyDown(KeyCode.G))
//            {
//                StatsPlayer itemManagerCharacter = GameObject.FindGameObjectWithTag("ItemManager").GetComponent<StatsPlayer>();

//                if (itemManagerCharacter.GetMaskStored() > 0)
//                {

//                    UIManager ui = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
                   
//                    ui.Qte();
//                    ui.HideButtonPress();
//                    itemManagerCharacter.DecresceAlcool();
//                    Time.timeScale = 1;
//                    qteEvento = false;

//                }

//            }



//    }

//    private void GoToHome()
//    {

//        this.transform.LookAt(homepointList[_indexHomePoint].position);
//        this.transform.position = Vector3.MoveTowards(transform.position, homepointList[_indexHomePoint].position, _speed * Time.deltaTime);
//        npcAnimationController.SetBool("Walking", true);
//        if (this.transform.position == homepointList[_indexHomePoint].position)
//        {
//            Destroy(this.gameObject);
//        }

//    }

//    public void MoveToWaypoints()
//    {
//        if (_walking)
//        {

//            this.transform.LookAt(waypointList[_indexWaypoint].position);
//            this.transform.position = Vector3.MoveTowards(transform.position, waypointList[_indexWaypoint].position, _speed * Time.deltaTime);
//            npcAnimationController.SetBool("Walking", true);

//            if (this.transform.position == waypointList[_indexWaypoint].position)
//                _indexWaypoint++;
//            if (_indexWaypoint >= waypointList.Count) _indexWaypoint = 0;

//        }
//        //npcAnimationController.SetBool("Walking", true);


//    }
//    private void OnTriggerEnter(Collider other)
//    {

//        if (!isMasked)
//        {
//            _walking = false;
//            npcAnimationController.SetBool("Walking", false);
//            this.transform.LookAt(other.transform.position);
//            if (Random.Range(0, 100) < 30)
//            {

//                Debug.Log("entrou na chance de 10%");
//                qteEvento = true;
//                DisplayMessengerButtonPress();
//                Time.timeScale = 0.1f;

//                StartCoroutine(Wait());
//            }
//        }


       

//    }
//    IEnumerator Wait()
//    {

//        yield return new WaitForSeconds(0.3f);
//        if (qteEvento)
//        {
//            StatusEffectPlayer sep = GameObject.FindGameObjectWithTag("Player").GetComponent<StatusEffectPlayer>();
//            sep.PermantDmgMultiply();
//        }
//        Time.timeScale = 1;

//    }
//    private void OnTriggerExit(Collider other)
//    {
//        _walking = true;
//    }



//    private void PutMask()
//    {
//        if (!isMasked)
//        {
            
//            StatsPlayer itemManagerCharacter = GameObject.FindGameObjectWithTag("ItemManager").GetComponent<StatsPlayer>();
//            NPCMask npcMaskRef = GetComponentInChildren<NPCMask>();


//            if (itemManagerCharacter.GetMaskStored() > 0)
//            {

//                itemManagerCharacter.AddPeopleSaved();
//                itemManagerCharacter.DecresceMask();
//                npcMaskRef.ActiveMask();
//                isMasked = true;
//                _walking = true;

//                DisplayMessengerOnScreen();
//            }
            

//        }

//    }
//    private void DisplayMessengerButtonPress()
//    {
//        UIManager ui = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
//        ui.ShowButtonPress();
//    }
//    private void DisplayMessengerOnScreen()
//    {
//        UIManager ui = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
//        ui.ShowMessage();
//    }
//}
