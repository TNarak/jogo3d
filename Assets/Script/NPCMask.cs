﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMask : MonoBehaviour
{
    public GameObject mask;
    // Start is called before the first frame update
    void Start()
    {
        mask.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ActiveMask()
    {
        mask.SetActive(true);
    }
}
