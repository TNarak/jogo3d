﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{

    public Transform PlayerTransform;
    private Quaternion _camRotation;

    private Vector3 _cameraOffset;
    [Range(0.1f, 1.0f)]
    public float SmoothFactor = 0.5f;
    public float RotatioSpeed = 200.0f;
    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;

        _cameraOffset = transform.position - PlayerTransform.position;
        _camRotation = transform.localRotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {

        //_camRotation.y += Input.GetAxis("Mouse X") * -1 * (-1);
        //_camRotation.x += Input.GetAxis("Mouse Y") * -1;

        //_camRotation.x = Mathf.Clamp(_camRotation.x, -60, 60);
        //transform.localRotation = Quaternion.Euler(_camRotation.x,_camRotation.y,_camRotation.z);


        Quaternion camTurnAngleY = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * (RotatioSpeed * Time.deltaTime), Vector3.up);
        Quaternion camTurnAngleX= Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * (RotatioSpeed * Time.deltaTime), Vector3.forward);
        

        _cameraOffset = camTurnAngleX * _cameraOffset;
        _cameraOffset = camTurnAngleY * _cameraOffset;

        Vector3 newPos ;
        newPos.x  = PlayerTransform.position.x + _cameraOffset.x;
        newPos.y  = PlayerTransform.position.y + _cameraOffset.y;
        newPos.z = PlayerTransform.position.z + _cameraOffset.z;

        transform.position = newPos;
        //transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
        transform.LookAt(PlayerTransform);

    }
}
